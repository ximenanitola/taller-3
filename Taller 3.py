#Importar la blibioteca Pandas en Python
#Calcular la mediana y la media de los datos almacenados en una lista de 100 elementos aleatorios entre -100 y 100  

import pandas as pd
import random

n=100
Lista = [random.randint(-100,100) for _ in range(n)]
print("Los datos son: ",Lista)

miSerie=pd.Series(Lista)
#print(miSerie)

#Calcular media
media=miSerie.mean()
print("La media de los datos es: ",media)

#Calcular mediana 
mediana=miSerie.median()
print("La mediana de los datos es: ",mediana)

